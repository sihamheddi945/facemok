<?php

namespace Database\Factories;

use App\Models\Task;

use App\Models\Phase;

use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name"=> $this->faker->sentence(3),
            "description"=>$this->faker->paragraph,
            "start_date"=>$this->faker->dateTime(),
            "end_date"=>$this->faker->dateTime(),
            "date_accomplishment"=>$this->faker->dateTime(),
            "status"=>$this->faker->randomElement(["done","in progress","to do","in review"]),
            "priority"=>$this->faker->randomElement(["Urgente","Haute","Normale","basse"]),
            "phase_id"=>Phase::factory()

        ];
    }
}
