<?php

namespace Database\Factories;

use App\Models\member;
use Illuminate\Database\Eloquent\Factories\Factory;

class memberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = member::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name"=>$this->faker->name,
            "username"=>$this->faker->userName,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
             "role"=> $this->faker->randomElement(["chef","developper","archetecter","designer"])
        ];
    }
}
