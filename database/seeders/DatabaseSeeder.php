<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\member;
use App\Models\Task;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
/*       \App\Models\member::factory(20)->create();
 */
      
      for ($i=0; $i <25 ; $i++) { 
        member::factory()
        ->hasAttached(Task::factory()->count(3))
        ->create();
      }
    
    }
}
