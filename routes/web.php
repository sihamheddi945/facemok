<?php

use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\hello;
use App\Http\Controllers\userController;
use  App\Http\Controllers\CategoriesController;
use Illuminate\Http\Request;
/* use  App\Http\Middleware\checkRole;  */
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/{any}', [hello::class, 'index'])->name("home")->where('any', '.*');


