<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/* $request->user()->currentAccessToken()->delete();
 */


 Route::post("/login",[Controllers\AuthController::class,"login"]);

 Route::post("/register",[Controllers\AuthController::class,"register"]);


Route::group(["middleware"=>'auth:sanctum'], function () {


    Route::get("/messages/{id}",[Controllers\MessageController::class,"getMessage"]);




});








