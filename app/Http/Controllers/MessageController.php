<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function getMessage($id)
    {
/*         return User::find($id)->messages();
 */  

       return response()->json(User::find($id)->messages()->get());
 
    }
}
