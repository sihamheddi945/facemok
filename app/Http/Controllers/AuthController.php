<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
       $v = $request->validate([
            "email"=>"required|email",
            "password"=>["required","string","max:8"]
        ]);
       if (!Auth::attempt($v)) {
                
                return response()->json([
                'message' => 'Invalid login details'
                  ], 401);
             }

      $user = User::where('email', $request['email'])->first();

      $token = $user->createToken('auth_token')->plainTextToken;

      return response()->json([
                 'access_token' => $token,
                 'token_type' => 'Bearer',
                 "message"=> "you logined successfully"
      ]);
    
    
    
    }

    
    public function register(Request $request)
    {
        $v = $request->validate([
            "email"=>"required|email|unique:users",
            "password"=>["required","string","max:8"],
            "first_name"=>["required","string"],
            "last_name"=>["required","string"],
            "birthday"=>["required","date"]

        ]);

        $v['password'] = Hash::make($v['password']);
         
        $user = User::create($v);

     $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
          'access_token' => $token,
          'token_type' => 'Bearer',
           'message'=>"you registered successfully"
        ]);




    }
}
