<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class checkSalary
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->salary>=45) {
            return redirect("/");
        }

        return $next($request);
    }
}
