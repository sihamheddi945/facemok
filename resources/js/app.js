require('./bootstrap');




import App from './views/app.vue'

import Vue from 'vue'

import vuetify from './vuetify'
import router from './routes';

const app = new Vue({
    vuetify,
    render: h => h(App),
    el: '#app',
    router

});

