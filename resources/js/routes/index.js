import Vue from 'vue'
import VueRouter from 'vue-router'

import home from "../views/components/home"
import Login from "../views/components/login"
import register from '../views/components/register'
import foreget from '../views/components/forget'
import password from '../views/components/password'
import news from '../views/components/autorised/news'


Vue.use(VueRouter)

const routes=[
    {
       path:"/news",
       component:news
    },
    {
        path:"/",
        component:home
    },
    {
        path:"/login",
        component:Login
    },
    {
        path:"/register",
        component:register
    },
    {
        path:"/foreget",
        component:foreget
    },
    {
        path:"/password/:id",
        component:password
    }
]


const router = new VueRouter({
    mode: 'history',
    routes:routes
})

export default router